package ca.echo35.spigot.syunpl;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public class SyunCore extends JavaPlugin {
	protected static Logger logger;
	protected static SyunMethods methods;
	protected static SyunCommands commands;

	public void onEnable() {
		this.logger = this.getLogger();
		this.methods = new SyunMethods(this);
		this.commands = new SyunCommands(this);
		this.getServer().getPluginManager().registerEvents(new SyunListeners(this), this);
		this.getCommand("ta").setExecutor(commands.ta);
		this.getCommand("cp").setExecutor(commands.cp);
		this.getServer().getLogger().info("SyunPLをロードしました");
	}
}
