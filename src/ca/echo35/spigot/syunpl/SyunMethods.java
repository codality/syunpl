package ca.echo35.spigot.syunpl;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

public class SyunMethods {
	private SyunCore core;
	private static HashMap<String, HashMap<UUID, Long>> instances = new HashMap<>();

	public SyunMethods(SyunCore core) {
		this.core = core;
	}

	public static String triggerTimer(String verb, String course, Player player) {
		if (!instances.containsKey(course))
			instances.put(course, new HashMap<>());
		HashMap<UUID, Long> instance = instances.get(course);
		if (verb.equals("off")) {
			if (!instance.containsKey(player.getUniqueId())) {
				return ChatColor.RED + "［引数エラー］開始してないタイマーを停止することが出来ません";
			}
			long difference = System.currentTimeMillis() - instance.get(player.getUniqueId());

			long minutes = difference / (1000 * 60) % 60;
			float seconds = difference / (1000f) % 60;
			String time = String.format("%d分 %.2f秒",
					minutes,
					seconds
			);
			instance.remove(player.getUniqueId());
			if (instance.isEmpty()) instances.remove(instance);
			return ChatColor.LIGHT_PURPLE + player.getName()
					+ ChatColor.GREEN + "さんが「" + course + "」を"
					+ ChatColor.LIGHT_PURPLE + time
					+ ChatColor.GREEN + "でクリアしました";
		} else if (verb.equals("on")) {
			instance.put(player.getUniqueId(), System.currentTimeMillis());
			return ChatColor.GREEN + "「" + course + "」アスレのタイマー開始";
		}
		return "［実行エラー］不明なエラーが起こりました";
	}

	public static String setCheckpoint(Player player, String identifier) {
		if (player.getInventory().firstEmpty() == -1) {
			return ChatColor.RED + "［失敗］ あなたのインベントリには余裕がありません";
		}

		ItemStack magmaCream = null;
		int slot = 0;
		for (ItemStack is : player.getInventory().getContents()) {
			if (is != null
					&& is.getType() == Material.MAGMA_CREAM
					&& isWarpCream(is)
					&& is.getItemMeta().getDisplayName().equals(identifier + "へ戻る")) {
				magmaCream = is;
				break;
			}
			slot++;
		}

		if (magmaCream == null) {
			slot = 0;
			for (ItemStack is : player.getInventory().getContents()) {
				if (is != null
						&& is.getType() == Material.MAGMA_CREAM
						&& ! isWarpCream(is)) {
					if (is.getAmount() > 1) slot = player.getInventory().firstEmpty();
					is.setAmount(is.getAmount() - 1);
					magmaCream = new ItemStack(Material.MAGMA_CREAM);
					break;
				}
				slot++;
			}
		}

		if (magmaCream == null) {
			return ChatColor.RED + "［失敗］ MagmaCreamを持ってコマンドを再送信してください";
		}

		ItemStack warpCream = magmaCream.clone();
		magmaCream.setAmount(magmaCream.getAmount() - 1);
		player.getInventory().setItem(slot, magmaCream.clone());

		warpCream.setAmount(1);
		ItemMeta im = warpCream.getItemMeta();
		im.setDisplayName(identifier + "へ戻る");
		Location loc = player.getLocation();
		String data = String.format("%s\u00aa%.2f,%.2f,%.2f\u00aa%.2f,%.2f", loc.getWorld().getName(), loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		im.setLore(Arrays.asList(new String[]{
				ChatColor.MAGIC + data
//				String.format(ChatColor.MAGIC + "ワールド：%s", loc.getWorld().getName()),
//				String.format(ChatColor.MAGIC + "座標：%.2f,%.2f,%.2f", loc.getX(), loc.getY(), loc.getZ()),
//				String.format(ChatColor.MAGIC + "角度：%.2f,%.2f", loc.getYaw(), loc.getPitch())
		}));

		warpCream.setItemMeta(im);
		player.getInventory().setItem(slot, warpCream);
		return ChatColor.GREEN + "［成功］ チェックポイントを作成しました";
	}

	public static boolean isWarpCream(ItemStack is) {
		if ((is == null)
				|| (is.getType() != Material.MAGMA_CREAM)
				|| (!is.hasItemMeta())
				|| (!is.getItemMeta().getDisplayName().contains("へ戻る")))
			return false;
		return true;
	}
}
