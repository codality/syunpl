package ca.echo35.spigot.syunpl;

import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.util.UUID;

public class SyunCommands {
	private SyunCore core;

	public SyunCommands(SyunCore core) {
		this.core = core;
	}

	public CommandExecutor ta = new CommandExecutor() {
		@Override
		public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
			if (s.equals("ta")) {
				if (!(commandSender instanceof BlockCommandSender || commandSender instanceof ConsoleCommandSender)) {
					commandSender.sendMessage(ChatColor.RED + "［権限エラー］このコマンドはコンソールやコマンドブロックしか使用できません");
					return false;
				}
				if (strings.length != 3) {
					commandSender.sendMessage(ChatColor.RED + "［文法エラー］正しい使い方は /ta <on|off> <アスレ名> <プレイヤー名>");
					return false;
				}
				String verb = strings[0];
				if (!(verb.equals("on") || verb.equals("off"))) {
					commandSender.sendMessage(ChatColor.RED + "［文法エラー］１引数目は on、off から一つを選んでください");
					return false;
				}
				String course = strings[1];
				Player player = core.getServer().getPlayer(strings[2]);
				if (player == null) {
					commandSender.sendMessage(ChatColor.RED + "［引数エラー］与えたプレイヤーが見つけられませんでした");
					return false;
				}
				UUID uuid = player.getUniqueId();
				if (player != null) {
					String message = core.methods.triggerTimer(verb, course, player);
					commandSender.sendMessage(message);
					if (!message.contains("エラー")) {
						if (message.contains("クリア")) core.getServer().broadcastMessage(message);
						else player.sendMessage(message);
					}
					return true;
				}
			}
			return false;
		}
	};

	public CommandExecutor cp = new CommandExecutor() {
		@Override
		public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
			if (s.equals("cp")) {
				if (!(commandSender instanceof BlockCommandSender || commandSender instanceof ConsoleCommandSender)) {
					commandSender.sendMessage(ChatColor.RED + "［権限エラー］このコマンドはコンソールやコマンドブロックしか使用できません");
					return false;
				}
				if (strings.length != 2) {
					commandSender.sendMessage(ChatColor.RED + "［文法エラー］正しい使い方は /cp <アスレ名> <プレイヤー名>");
					return false;
				}
				Player player = core.getServer().getPlayer(strings[1]);
				if (player != null) {
					String message = core.methods.setCheckpoint(player, strings[0]);
					commandSender.sendMessage(message);
					if (!commandSender.equals(player)) {
						player.sendMessage(message);
					}
					return true;
				}
			}
			return false;
		}
	};


}
