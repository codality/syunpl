package ca.echo35.spigot.syunpl;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class SyunListeners implements Listener {
	SyunCore core;

	public SyunListeners(SyunCore core) {
		this.core = core;
	}

	@EventHandler
	public void onRightClickMagmaCream(PlayerInteractEvent event) { // MagmaCreamを持ちながら右クリックときに
		if ((event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
				&& core.methods.isWarpCream(event.getPlayer().getInventory().getItemInMainHand())) {
			ItemStack warpCream = event.getPlayer().getInventory().getItemInMainHand();
			ItemMeta im = warpCream.getItemMeta();
			String[] data = im.getLore().get(0).replace(ChatColor.MAGIC + "", "").split("\u00aa");
			String world = data[0];
			String[] coords = data[1].split(",");
			String[] angle = data[2].split(",");
			try {
				Location loc = new Location(
						core.getServer().getWorld(world),
						Double.parseDouble(coords[0]),
						Double.parseDouble(coords[1]),
						Double.parseDouble(coords[2]),
						Float.parseFloat(angle[0]),
						Float.parseFloat(angle[1])
				);
				event.getPlayer().teleport(loc);
				event.getPlayer().sendMessage(ChatColor.GREEN + "［成功］ ワープされました");
			} catch (Exception e) {
				// アイテムが偽物なの可能性高い
				e.printStackTrace();
			}
		}
	}

	@EventHandler
	public void onPlayerDrop(PlayerDropItemEvent event) {
		ItemStack is = event.getItemDrop().getItemStack();
		if (core.methods.isWarpCream(is)) {
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("");
			im.setLore(new ArrayList<>());
			is.setItemMeta(im);
		}
	}
}
